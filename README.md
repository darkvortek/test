Типы сутрудников - обозначения в коде:


программист - programmer

дизайнер - designer

тестировщик - tester

менеджер - manager




Типы навыков - обозначения в коде:

писать код - writeCode

тестировать - testCode

общаться с менеджером - communication

рисовать - drawing

ставить задачи - setTasks


Команды:

php bin/console company:employee <тип сотрудника>

php bin/console employee:can <тип сотрудника> <тип навыка>
