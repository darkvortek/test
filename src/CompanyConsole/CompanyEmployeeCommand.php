<?php
namespace App\CompanyConsole;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class CompanyEmployeeCommand extends Command
{

    protected static $defaultName = 'company:employee';

    protected function configure()
    {
        $this->addArgument('worker', InputArgument::REQUIRED, 'Worker position in company');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $position = $input->getArgument('worker');

        $worker_skills = Processor::getWorkerSkills($position);
        $output->writeln($worker_skills);

        return true;
    }
}