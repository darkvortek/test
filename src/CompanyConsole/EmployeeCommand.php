<?php
namespace App\CompanyConsole;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class EmployeeCommand extends Command
{

    protected static $defaultName = 'employee:can';

    protected function configure()
    {
        $this->addArgument('worker_position', InputArgument::REQUIRED, 'Worker position in company');
        $this->addArgument('skill', InputArgument::REQUIRED, 'Worker skill');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $position = $input->getArgument('worker_position');
        $skill = $input->getArgument('skill');

        $check = Processor::checkWorkerSkill($position, $skill);
        $result_text = ($check) ? 'true' : 'false';
        $output->writeln($result_text);

        return true;
    }
}