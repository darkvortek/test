<?php
namespace App\CompanyConsole;

use App\CompanyConsole\Classes\Workers\WorkerAbstract;

class Processor
{

    public static function getWorker($position) : WorkerAbstract
    {
        $position = '\App\CompanyConsole\Classes\Workers\\' . ucfirst(mb_strtolower($position));
        return new $position();
    }
    public static function getWorkerSkills($position) : array
    {
        $worker = self::getWorker($position);

        return $worker->generateSkillsText();
    }

    public static function checkWorkerSkill($position, $checked_skill) : bool
    {
        $worker = self::getWorker($position);
        $worker_skills = $worker->getSkills();

        foreach ($worker_skills as $skill) {
            if (mb_strtolower($skill->getName()) == mb_strtolower($checked_skill)) {
                return true;
            }
        }

        return false;
    }
}