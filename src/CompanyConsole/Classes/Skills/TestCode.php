<?php
namespace App\CompanyConsole\Classes\Skills;

use App\CompanyConsole\Classes\Skills\SkillAbstract;

class TestCode extends SkillAbstract
{
    private $name = 'testCode';

    public function getName()
    {
        return $this->name;
    }

    public function getText() : string
    {
        return "- code testing";
    }
}