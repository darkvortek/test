<?php
namespace App\CompanyConsole\Classes\Skills;

use App\CompanyConsole\Classes\Skills\SkillAbstract;

class WriteCode extends SkillAbstract
{
    private $name = 'writeCode';

    public function getName()
    {
        return $this->name;
    }

    public function getText() : string
    {
        return "- code writing";
    }
}