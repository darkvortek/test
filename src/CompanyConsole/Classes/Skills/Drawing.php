<?php
namespace App\CompanyConsole\Classes\Skills;

use App\CompanyConsole\Classes\Skills\SkillAbstract;

class Drawing extends SkillAbstract
{
    private $name = 'drawing';

    public function getName()
    {
        return $this->name;
    }

    public function getText() : string
    {
        return "- {$this->name}";
    }
}