<?php
namespace App\CompanyConsole\Classes\Skills;

use App\CompanyConsole\Classes\Skills\SkillAbstract;

class Communication extends SkillAbstract
{
    private $name = 'communication';

    public function getName()
    {
        return $this->name;
    }

    public function getText() : string
    {
        return "- {$this->name} with manager";
    }
}