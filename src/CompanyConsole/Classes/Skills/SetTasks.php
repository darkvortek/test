<?php
namespace App\CompanyConsole\Classes\Skills;

use App\CompanyConsole\Classes\Skills\SkillAbstract;

class SetTasks extends SkillAbstract
{
    private $name = 'setTasks';

    public function getName()
    {
        return $this->name;
    }

    public function getText() : string
    {
        return "- set tasks";
    }
}