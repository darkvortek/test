<?php
namespace App\CompanyConsole\Classes\Skills;

abstract class SkillAbstract
{
    private $name = '';

    abstract public function getName();


    abstract public function getText();
}