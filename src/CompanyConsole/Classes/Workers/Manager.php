<?php
namespace App\CompanyConsole\Classes\Workers;

use App\CompanyConsole\Classes\Skills\SetTasks;

class Manager extends WorkerAbstract
{
    public $skills = array();

    public function __construct()
    {
        $this->skills = array(
            new SetTasks()
        );
    }
}