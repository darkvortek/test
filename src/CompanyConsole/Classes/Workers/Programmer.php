<?php
namespace App\CompanyConsole\Classes\Workers;

use App\CompanyConsole\Classes\Skills\WriteCode;
use App\CompanyConsole\Classes\Skills\TestCode;
use App\CompanyConsole\Classes\Skills\Communication;

class Programmer extends WorkerAbstract
{
    public $skills = array();

    public function __construct()
    {
        $this->skills = array(
            new WriteCode(),
            new TestCode(),
            new Communication()
        );
    }

    public function getSkills() : array
    {
        return $this->skills;
    }
}