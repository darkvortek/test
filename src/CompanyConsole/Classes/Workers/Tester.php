<?php
namespace App\CompanyConsole\Classes\Workers;

use App\CompanyConsole\Classes\Skills\Communication;
use App\CompanyConsole\Classes\Skills\TestCode;
use App\CompanyConsole\Classes\Skills\SetTasks;

class Tester extends WorkerAbstract
{
    public $skills = array();

    public function __construct()
    {
        $this->skills = array(
            new TestCode(),
            new Communication(),
            new SetTasks()
        );
    }
}