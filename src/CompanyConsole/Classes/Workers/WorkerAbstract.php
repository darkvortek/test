<?php
namespace App\CompanyConsole\Classes\Workers;

abstract class WorkerAbstract
{
    public $skills = array();

    public function getSkills() : array
    {
        return $this->skills;
    }

    public function generateSkillsText() : array
    {
        $result = array();
        foreach($this->skills as $skill) {
            if($skill instanceof \App\CompanyConsole\Classes\Skills\SkillAbstract) {
                $result[] = $skill->getText();
            }
        }

        return $result;
    }
}