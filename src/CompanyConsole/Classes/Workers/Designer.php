<?php
namespace App\CompanyConsole\Classes\Workers;

use App\CompanyConsole\Classes\Skills\Communication;
use App\CompanyConsole\Classes\Skills\Drawing;

class Designer extends WorkerAbstract
{
    public $skills = array();

    public function __construct()
    {
        $this->skills = array(
            new Drawing(),
            new Communication()
        );
    }
}